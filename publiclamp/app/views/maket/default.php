<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <script src="/publiclamp/publicstirpes/scripts/jquery.js"></script>
  <!--  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
   -->

    <link rel="shortcut icon" href="#">
    <link rel="stylesheet" href="/publiclamp/app/views/maket/mainPage.css">

    <!--  ---------------------------------------------------------------------------   -->

    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
     -->

    <link rel="stylesheet" href="/publiclamp/publicstirpes/styles/stboot/css/bootstrap.css" >
    <link rel="stylesheet" href="/publiclamp/publicstirpes/styles/stboot/css/bootstrap-grid.css" >

    <!--            -----filter-choice-----                  -->
    <link href="https://fonts.googleapis.com/css?family=Jura" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Forum" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
</head>
<body>

<?php echo $content; ?>

<script src="/publiclamp/publicstirpes/scripts/specparam.js"></script>
<script src="/publiclamp/publicstirpes/scripts/login.js"></script>

<script src="publiclamp/publicstirpes/styles//stboot/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
