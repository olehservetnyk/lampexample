
<div class="chief">

    <div class="body-roof">
        <div class="contact">
            <div class="text-contact-motto">
                зробимо світ світлішим
            </div>
            <div class="maquette">
                ("maquette")
            </div>
            <div class="text-telephone">
                <a href="javascript:void(0);">тел. 000-000-00-00
                </a>
            </div>
        </div>
    </div>

    <div class="body-header">
        <div class="logo">
            <img src="/publiclamp/publicstirpes/images/main-container/catalog/beLight_80x80.jpg" alt="" class="img-logo">
        </div>

        <div class="service">

            <div class="common-data">
                <div class="text-common-delivery">
                    <a href="javascript:void(0);">Доставка та оплата
                    </a>
                </div>
                <div class="line-left">
                </div>
                <div class="text-common-guarantee">
                    <a href="javascript:void(0);">Гарантія
                    </a>
                </div>
                <div class="line-right">
                </div>
                <div class="text-common-order">
                    <a href="javascript:void(0);">Відстеження замовлення
                    </a>
                </div>
            </div>

            <div class="search">
                <form id ="search-box" method="POST" action="" > <!-- method="get" -->
                <div class="circle-search" >
                     <input type="text" placeholder="Пошук" name="str" alt="Пошук" >
                    <div class="button-search" id="srh">
                        <a href="javascript:void(0);">  <input  type="image"  src="/publiclamp/publicstirpes/images/body-header/service/search/sr22.png " style="vertical-align: bottom; padding: auto;"  >
                        </a>
                    </div>
                </div>
                </form>
            </div>
            <div class="person-data">
                <div class="text-person-basket">
                    <a href="javascript:void(0);"> До Кошику
                    </a>
                </div>
                <div class="text-person-entry">
                    <a href="/account/login">Вхід
                    </a>
                </div>

            </div>

        </div>

    </div>

    <div class="main-container">
        <div class="catalog">
            <div class="basic-kind">
                <div class="text-basic-kind">
                    Лампи:)
                </div>
                <div class="line-vertical-basickind">
                </div>
                <div class="energySaving">
                    <a id="idenergySav" href="javascript:void(0);"" >Енергоощадні</a>
                </div>
                <div class="halogen">
                    <a id="idhalog"  href="javascript:void(0);">Галогенні</a>
                </div>
                <div class="fluorescent">
                    <a id="idfluor"  href="javascript:void(0);"">Люмінісцентні</a>
                </div>
                <div class="led">
                    <a id="idled"  href="javascript:void(0);"">Світлодіодні</a>
                </div>
                <div class="filament">
                    <a id="idfilam"  href="javascript:void(0);"">Філаментні</a>
                </div>
            </div>
            <div class="specific-param">
                <div id="idspecparam">
                    <img src="/publiclamp/publicstirpes/images/main-container/catalog/specParam/lantern.jpg">
                </div>
            </div>
        </div>
        <div class="screen-market">

            <div class="circle-show">

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box-slider">

                                <div id="slide" class="carousel slide" data-ride="carousel"   style="width:480px; height: 200px;">
                                    <ol class="carousel-indicators">
                                        <li data-target="#slide" data-slide-to="0" class="active"></li>
                                        <li data-target="#slide" data-slide-to="1" class="active"></li>
                                        <li data-target="#slide" data-slide-to="2" class="active"></li>
                                    </ol>
                                    <div class="carousel-inner" style=" height: 200px;" >
                                        <div class="carousel-item active">
                                            <img src="/publiclamp/publicstirpes/images/main-container/screen-market/circle-show/house.jpg" alt="" class="d-block " >
                                            <div class="carousel-caption d-none d-md-block">
                                                <h2 style="color:rgb(0,0,0)">example</h2>
                                                <p style="color:rgb(0,0,0)">promotion</p>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <img src="/publiclamp/publicstirpes/images/main-container/screen-market/circle-show/garden.jpg" alt="" class="d-block " >
                                            <div class="carousel-caption d-none d-md-block">
                                                <h2 style="color:rgb(0,0,0)">example</h2>
                                                <p style="color:rgb(0,0,0)">promotion</p>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <img src="/publiclamp/publicstirpes/images/main-container/screen-market/circle-show/artowl.jpg" alt="" class="d-block " >
                                            <div class="carousel-caption d-none d-md-block">
                                                <h2 style="color:rgb(0,0,0)">example</h2>
                                                <p style="color:rgb(0,0,0)">promotion</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#slide" role="button" data-slide="prev" class="carousel-control-prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    </a>
                                    <a href="#slide" role="button" data-slide="next" class="carousel-control-next">
                                        <span class="carousel-control-next-icon" aria-hidden="true" ></span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="field-show">
                <?php
                echo("Brand='philips' :");echo ("</br>");
                echo("sql:'SELECT name FROM nameitem  WHERE idBrand=4'");echo ("</br>");
                ;echo ("</br>");
                foreach ($substance as $k0 => $v0) {
                    foreach ($v0 as $k1 => $v1) {
                        foreach ($v1 as $k2 => $v2) {
                            echo ($v2);echo '</br>';
                        }
                    }
                }
                ?>
            </div>
        </div>

        <div class="notice">
           <div class="notice-setting">
               <a href="javascript:void(0);">
               <div class="option-block">
                   <div class="option-block-line-1">
                   </div>
                   <div class="option-block-line-2">
                   </div>
                   <div class="option-block-line-3">
                   </div>
               </div>

               <div class="option-column">
                   <div class="option-column-within">
                   </div>
               </div>
               </a>
           </div>
            <div class="notice-market">
                <img src="/publiclamp/publicstirpes/images/main-container/notice/noticeMarket/kerosene_lamp.jpg">
            </div>
        </div>
    </div>

    <div class="footer-container">
        <div class="logo-brand">
        </div>
    </div>

    <div class="basement">
        <div class="common-info">

        </div>
    </div>

</div>

