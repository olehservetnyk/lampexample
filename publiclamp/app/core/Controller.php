<?php
/**
 * Created by PhpStorm.
 * User: sun
 * Date: 7/19/2018
 * Time: 3:21 PM
 */

abstract class Controller
{

    public $driveways;
    public $view;
    public $model;

    public function __construct($driveways)
    {
        $this->driveways =$driveways;
        $this->view =new View($driveways);
        $this->model =$this->modelLuggage($driveways['controller']);

    }

    public function modelLuggage($trimModel){
        $path = ucfirst($trimModel);
        if (class_exists($path)) {
            return new $path;
        }
    }
}
