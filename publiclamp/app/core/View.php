<?php
/**
 * Created by PhpStorm.
 * User: sun
 * Date: 7/2/2018
 * Time: 9:37 AM
 */

class View
{
    public $driveways;
    public $conveyView;
    public $layout = "default";

    public function __construct($driveways)
    {
        $this->driveways = $driveways;
        $this->conveyView = $driveways['controller'] . '/' . $driveways['action'];
    }

    public function represent($title, $substance = [])
    {
        extract($substance);
        $path = 'publiclamp/app/views/' . $this->conveyView . ".php";
        if (file_exists($path)) {
            ob_start();
            require $path;
            $content = ob_get_clean();
            require '/publiclamp/app/views/maket/' . $this->layout . ".php";
        }
    }

    public function redirect($url){
        header('location: ' . $url);
        exit;
    }

    public static function errorCode($code){
        http_response_code($code);
        $path ='publiclamp/app/views/errors/' . $code . ".php";
        if (file_exists($path)){
            require $path;
        } //else {echo "View is not found:".$path;}
        exit;
    }

    public function location($url){
        exit(json_encode(['url' => $url]));
    }

    public function message($status,$message){
        exit(json_encode(['status' => $status, 'message' => $message]));
    }

}