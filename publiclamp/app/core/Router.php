<?php
/**
 * Created by PhpStorm.
 * User: sun
 * Date: 6/29/2018
 * Time: 10:09 AM
 */

class Router
{
    private $way;
    function __construct()
    {
        $this->way =require_once '/publiclamp/app/config/waybill.php';
        foreach ($this->way as $item => $value){
            $this->addRoute($item,$value);
        }
    }

    protected $routes =[];
    protected $driveways =[];

    public function addRoute($road,$ride){
        $road ='~^' . $road . '$~';
        $this->routes[$road] =$ride;
    }

    public function DetectEqualWay(){
        $url =trim($_SERVER['REQUEST_URI'],'/');
        foreach ($this->routes as $road => $ride){
            if(preg_match($road,$url,$matches)){
                $this->driveways =$ride;
                return true;
            }
        }return false;
    }

    public function step(){
        if ($this->DetectEqualWay()){
            $path =ucfirst($this->driveways['controller'] . "Controller");
            if (class_exists("$path")){
                $action =$this->driveways['action'] . 'Action';
                if(method_exists($path,$action)){
                    $controller = new $path($this->driveways);
                    $controller->$action();
                }else {
                    View::errorCode(404);}
            }else{
                echo "</br>";echo "not found controller:".$path;echo "</br>";
                View::errorCode(404);}
        }else{
            View::errorCode(403);}
    }


}