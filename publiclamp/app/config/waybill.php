<?php
return [

    ''=> [
        'controller'=>'main',
        'action'=>'index',
    ],
    'account/register'=> [
        'controller'=>'account',
        'action'=>'register'
    ],
    'account/login'=> [
        'controller'=>'account',
        'action'=>'login'
    ],
    'cart/basket'=> [
        'controller'=>'cart',
        'action'=>'basket'
    ],
    'product/item'=> [
        'controller'=>'product',
        'action'=>'item'
    ],
    'main/search'=> [
        'controller'=> 'search',
        'action'=>'find',
    ]

];