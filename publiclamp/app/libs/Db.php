<?php

//namespace libs;
class Db
{
   protected $db;

   public function __construct()
    {
       $configDb =require '/publiclamp/app/config/db.php';

               $this->db =new PDO('mysql:host='.$configDb['host'].';'
           .'dbname='.$configDb['dbname'].'',
           $configDb['user'], $configDb['password']);
    }




    public  function Qquery($sgl, $params =[]){

        $stmt =$this->db->prepare($sgl);
        if (!empty($params)){
            foreach ($params as $key => $val){
                $stmt->bindValue(':'.$key,$val);
            }
        }
        $stmt->execute();
        return $stmt;
    }

    public function row($sgl,$params =[]){
        $result =$this->Qquery($sgl,$params);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function column($sgl,$params =[]){
        $result =$this->Qquery($sgl,$params);
        return $result->fetchColumn();
    }


}
