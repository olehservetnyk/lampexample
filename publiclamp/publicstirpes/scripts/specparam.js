
/*
*/
$(document).ready(function () {

  $('#idenergySav').click(function () {
      $("#idspecparam").fadeOut(500,"swing",function (){
          $("#idspecparam").load("/publiclamp/app/views/main/filter/ch_energysaving.html");
      });
      $("#idspecparam").fadeIn(500,"swing");
  });


  $('#idhalog').click(function () {

      $("#idspecparam").fadeOut(500,"swing",function (){
          $("#idspecparam").load("/publiclamp/app/views/main/filter/ch_halogen.html");
      });
      $("#idspecparam").fadeIn(500,"swing");
  });


  $('#idfluor').click(function () {
      $("#idspecparam").fadeOut(500,"swing",function (){
          $("#idspecparam").load("/publiclamp/app/views/main/filter/ch_fluorescent.html");
      });
      $("#idspecparam").fadeIn(500,"swing");
  });

  $('#idled').click(function () {
      $("#idspecparam").fadeOut(500,"swing",function (){
          $("#idspecparam").load("/publiclamp/app/views/main/filter/ch_led.html");
      });
      $("#idspecparam").fadeIn(500,"swing");
  });

  $('#idfilam').click(function () {
      $("#idspecparam").fadeOut(500,"swing",function (){
          $("#idspecparam").load("/publiclamp/app/views/main/filter/ch_filament.html");
      });
      $("#idspecparam").fadeIn(500,"swing");
  });


});
/*

*/