<?php
define('CORE','publiclamp/app/core/');
define('CONTR','publiclamp/app/controllers/');
define('DB','publiclamp/app/libs/');
define('MODEL','publiclamp/app/models/');

spl_autoload_register(function ($class){

    $file = CORE ."$class.php";
    if(is_file($file)){
        require_once $file;
    }

    $file = CONTR ."$class.php";
    if(is_file($file)){
        require_once $file;
    }

    $file = DB ."$class.php";
    if(is_file($file)){
        require_once $file;
    }

    $file = MODEL ."$class.php";
    if(is_file($file)){
        require_once $file;
    }
});

$router =new Router;
$router->step();